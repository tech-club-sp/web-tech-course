# Intro Git

Git es un control de versiones moderno. Un control de versiones es un sistema que registra los cambios realizados en tus archivos a lo largo del tiempo, de modo que puedas recuperar versiones específicas más adelante. Fue desarollado para archivos de código fuente pero en realidad puedes hacer lo mismo con casi cualquier tipo de archivo que encuentres en una computadora.

Ademas Git tiene funciones que permiten trabajar en conjunto y en diferentes versiones del proyecto al mismo tiempo. Normalmente se usa una base de datos (o en terminos de Git un _repository_) remoto, significa en un servidor. Eso sirve también como respaldo de datos.

Hay dos grandes proveedores de estos repositories remotos: GitHub y GitLab. Aquí vamos a aprovechar la oferta gratuita de GitLab.

Puedes encontrar un libro en línea que cubre todo sobre git aquí [Git Book (es)](https://git-scm.com/book/es/v2). Por ahora vamos a centrarnos en lo básico.

Para empezar necesitamos instalar el programa `git`: 
```
sudo apt-get install git
```