# El enlace `<a>`

Unas de las propriedades más importantes de HTML es la posibilidad de enlazar páginas entre ellas. Eso se hace a través de los enlaces o _links_.

## Enlazar otra página

Esto lo hemos visto ya muchas veces en este curso: un enlace a otra página. Como por ejemplo [este enlace a la página de mozilla sobre los elementos HTML](https://developer.mozilla.org/es/docs/Web/HTML/Elemento).

El código para este enlace se ve así:
```html
<a href="https://developer.mozilla.org/es/docs/Web/HTML/Elemento">este enlace a la página de mozilla sobre los elementos HTML</a>
```

El nombre del elemento es `a` por ancla o _anchor_ en inglés. El atributo `href` contiene la URL de la página que se va a cargar usando al enlace.

## Como se construye una URL

Vamos a ver las partes communes de una URL. Empiezan por el protocolo, en este caso es `https`. El protocolo define cómo habla el navegador con el servidor quien almacena la página. Luego viene `://` para separar el protocolo del nombre del servidor. El servidor tiene el nombre `developer.mozilla.org`. Después sigue el nombre del archivo que contiene la página. Este nombre viene con la lista de carpetas que hay que pasar para llegar al archivo. Eso se llama el _path_. En nuestro ejemplo de arriba es `/es/docs/Web/HTML/Elemento` el camino completo para llegar a la página.

Si quieres dar estructura a tu página, puedes dividir el contenido en varios archivos y usar enlaces para navegar de una página a la otra. Para esos enlaces no tienes que especificar el protocolo ni el servidor. Si los archivos están en la misma carpeta, un enlace de la siguiente forma es suficiente:

```html
<a href='capitulo_dos.html'>Capitulo 2</a>
```

Ahora tienes las herramientas para construir una página más grande y bien estructurada. Usalo en tu proyecto!

## Qué puerta tomamos?

Algunas URLs contienen lo que llamamos el _port_. Eso siempre es necesario si el servidor comunica con un port diferente de lo que es el estandard para el protocolo usado. El estandard para https es port 443 y para http es port 80.

Un ejemplo típico de una URL con port sería `http://www.ejemplo.com:8080/index.html`. Esta puede ser una página completamente diferente de la que está por ejemplo en `http://www.ejemplo.com/index.html`. Lo último es igual a `http://www.ejemplo.com:80/index.html`.

## Enlaces para saltar en la misma página

El atributo `href` puede también contener un fragmento de URL. Eso es un nombre precedido por el símbolo de número (#). El nombre es el `id` de otro elemento en la página actual.

Ejemplo:
```html
<h3 id='algo_chido'>Algo Chido</h3>
...
Quieres ver <a href='#algo_chido'>Algo Chido</a>?
```

En este ejemplo puedes saltar al capitulo 'Algo Chido' usando el enlace. El enlace y el capítulo se encuentran en la misma página.

## Atributo `id`

`id` es un atributo que podemos poner a todos los elementos para darles una identificación única. No debemos nunca usar la misma `id` dos veces en una página.


