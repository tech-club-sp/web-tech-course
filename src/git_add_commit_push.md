# Git add, commit, push y status

Vamos a aprender cómo agregar cambios en nuestro proyecto al repository de Git. Después de cada paso puedes checar el estatus de tu proyecto con `git status`.

## `add` al área de preparación

Trabajamos en nuestro working directory, en el cual estamos libres para probar y usarlo como cualquier folder. En el momento en que queremos agregar un cambio al proyecto agregamos los archivos nuevos o con cambios al área de preparación o _staging area_. Para eso sirve el comando `git add`. Por ejemplo `git add index.html` agrega el archivo `index.html` al staging area.

## `commit` al repository local

Cunado tienes el staging area preparado con los cambios que quieres comprometer usas el comando `git commit`. Eso genera un _commit_. Cada commit tiene un número, un autor con nombre y correo, una descripción (_commit message_) y obviamente los cambios que estaban en el staging area. Con la opción `-m` podemos directamente agregar un commit message, eso evita que se abra otro editor:
```shell
git commit -m"fixed spelling errors"
```

Para agregar todos los cambios (menos nuevos archivos) **sin** preparar el staging area:
```shell
git commit -a -m"fixed spelling errors"
```

En la respuesta a este comando puedes ver el número del commit, `0d31633` en este ejemplo, el número de líneas que se cambiaron, etc:
```
[master 0d31633] test
 1 file changed, 0 insertions(+), 0 deletions(-)
 ...
 ```

Ahora nuestro cambio queda guardado en el local repository (te recuerdas de la carpeta `.git` ? ).

## `push` al repository remoto

Para que el commit esté visible en el remote repository necesitamos 'empujarlo' con el comando `git push`. Si hicimos todo bien en el capítulo [Crear repository](./crear_repository.md) vamos a ver como resultado:
```
Counting objects: 2, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (2/2), 236 bytes | 236.00 KiB/s, done.
Total 2 (delta 0), reused 0 (delta 0)
To gitlab.com:tech-club-sp/member_reps/mi_proyecto_chido.git
   d12d6ba..0d31633  master -> master
```
Aquí vemos que el número de nuestro commit aparece otra vez, indicando que ya está en el remote repository.

Para comprobar podemos ver todos los commits en remote con un enlace parecido a este. Solo tienes que cambiar el nombre del proyecto: https://gitlab.com/tech-club-sp/member_reps/mi_proyecto_chido/-/commits/master
Si encuentras el commit que hiciste puedes elejirlo y ver todo la información que se guardó.

Recomiendo hacer commits para cada conjunto de cambios que van juntos y dejan el proyecto en un estado bueno. No olvides el push al fin, sino el commit se queda sólo en tu computadora.

## `master` y el resto

Quizas ya has visto varias veces la palabra `master` en el output de Git. Eso se refiere a la rama principal de tu proyecto y por ahora sólo vamos a trabajar en ella. Más sobre el tema en [Ramificaciones en Git](https://git-scm.com/book/es/v2/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F).

Git es muy flexible y se puede usar en situaciones muy diferentes. Te puedes dar una idea de lo que hay aparte de lo que hemos visto con el comando `git --help` y si tienes más dudas sobre un sub comando puedes leer la ayuda por ejemplo con `git commit -h`.

## Entonces, qué tengo que recordar?
Para cada cambio o nuevo archivo hay que ejecutar
```sh
git add NOMBRE_DE_ARCHIVO
```
y luego agregar al repositorio y subirlo a remote:
```
git commit -m"una descripción del cambio"
git push
```

## Este curso esta en GitLab también!

En https://gitlab.com/tech-club-sp/web-tech-course encuentras el código fuente del este curso. Si encuentras un error podrias corregirlo directamente en GitLab :-) Si tienes dudas de como hacerlo, contáctame!