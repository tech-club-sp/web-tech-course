- stackoverflow
- gitter
- usar linux
  - terminal
  - win
- editor
  - inst code
- init proyecto
  - home folder
  - mkdir
  - start code
  - index.html
  - verlo en el navegador
- git
- html
  - simple content stuff
- Css
  - some design
- JS
  - console.log
  - dev tools
  - interacción con el document
    - colores? distancias
  - conceptos del lenguaje
  - jquery
  - algoritmo
- Vue ?


## Quien es "root" y que tiene que ver con mi usuario?

En los sistemas operativos comunes hay un concepto que se llama derechos o _permissions_. Esos permissions definen quien puede hacer que en el sistema. Por ejemplo instalar programas, borrarlos o cambiar el sistema operativo requieren permissions especiales. Durante la instalación creaste un usuario en tu sistema. Este usuario no tiene esos derechos. Eso es para proteger tu sistema de errors tuyas o un mal comportamiento de alguna software que tengas.

También hay otro usuario que se crea automáticamente. Tradicionalmente en Linux ese usuario se llama _root_, también se refiere a el como _super user_. root puede hace todo en tu sistema. Leer todas las carpetas, borrar todo, etc. Para que tu puedas instalar una programa tienes que tener los derechos de root. 