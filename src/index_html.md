
## index.html

Ahora haremos un archivo con nuestro editor. Seguimos en el mismo terminal de la sección anterior (ya cambiamos a nuestra carpeta) y escribimos
```shell
code .
```
El `.` se refiere a la carpeta actual. Si instalaste el editor correctamente se va a abrir ahora. En el editor creas con `Ctrl+N` un nuevo archivo. También puedes usar el mouse e irte a `File -> New File`.

🥳 Y aquí empieza la magia. Ahora estas haciendo una página web!

Escribe lo siguiente en el archivo nuevo:
```html
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Mi proyecto chido</title>
  </head>
  <body>
    Hola mundo, este es mi proyecto chido!
  </body>
</html>
```

Lo vamos a guardar con `Ctrl+S`. Como nombre para el archivo usamos `index.html`, es una convención que cada página web tenga un archivo `index.html`.

En el capítulo sobre HTML vamos a ver qué significa todo lo que escribimos y qué más podemos hacer con HTML.
