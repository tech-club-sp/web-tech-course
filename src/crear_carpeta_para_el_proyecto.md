# Crear una carpeta para el proyecto

Todos los archivos tienen que vivir en una carpeta (_folder_ o _directory_ en inglés). Asumimos que vas a llamar el proyecto `mi_proyecto_chido`. Usamos `_` en lugar de espacios, porque no es recomendable tener espacios ni caracteres raros en nombres de archivos o carpetas.

En esta sección vamos a usar algunos comandos en el terminal:

- `mkdir` - _make directory_ - crear una carpeta
- `cd` - _change directory_ - cambiar a otra carpeta
- `ls` - _list_ - mostrar todo el contenido de una carpeta

Abrimos un terminal y creamos la carpeta
```shell
mkdir mi_proyecto_chido
```
Puedes ver si la carpeta se creó con éxito con el comando
```shell
ls
```
Va a aparecer una lista de carpetas y archivos, ahí deberías ver la nueva carpeta.

Ahora vamos a entrar a la nueva carpeta
```shell
cd mi_proyecto_chido
```
Un pequeño truco para trabajar más rápido: Puedes dejar que se completen automáticamente los nombres de carpetas y archivos! Si empiezas con el comando de arriba `cd mi` y presionas `Tab` se va a completar todo el nombre del archivo. en caso de que hubiera más opciones que inicien con las mismas letras, al presionar 2 veces `Tab` puedes ver todas las opciones.

En la siguiente sección vamos a crear un archivo aquí. Es importante que te quedes en esta carpeta.
