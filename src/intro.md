# Intro

Aquí vamos a aprender sobre las tecnologías que hacen funcionar a la _web_. El enfoque inicial lo vamos a poner en las herramientas que usa un desarrollador de software y en los lenguajes para programar una página web. Vamos a instalar y usar Linux como sistema operativo, VS Code como editor y varias otras herramientas sobre la marcha. Nuestra primera meta va a ser una pequeña paǵina que podremos ver en nuestro navegador. Luego vamos a orientarnos hacia la programación en JavaScript. 

Después veremos a dónde nos lleva el proyecto ...

Para seguir este curso necesitas una compu, internet y ganas de aprender :-)
