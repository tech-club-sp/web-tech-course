# Incluir imagenes

Lo que conozemos como Internet empezo con texto y enlaces. Las páginas se veían muy sencillas.

Ahora las páginas contienen mucho más que sólo texto. Una parte importante de la aparencia moderna son las imágenes.

Hoy vamos a probar algo nuevo. Como tarea estudien la clase sobre [Imágenes en HTML](https://developer.mozilla.org/es/docs/Learn/HTML/Multimedia_and_embedding/Images_in_HTML). Esa clase es parte de un curso sobre HTML que puede ser complementario a lo que estamos haciendo aquí. 

Usa imágenes en tu proyecto!

