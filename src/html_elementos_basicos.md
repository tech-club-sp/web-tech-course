# Elementos basicos

Una buena referencia en español para todos los elementos es https://developer.mozilla.org/es/docs/Web/HTML/Elemento .

Para probar los nuevos elementos que van a conocer aquí puedan usar el proyecto que ya empezaron o usar un servicio en línea que te permite editar un documento HTML y ver el efecto inmediatamente: [Codepen](https://codepen.io/pen/)

Un elemento se abre con una etiqueta o _tag_ `<>` y se cierre con otro tag `</>`. En este ejemplo queremos hacer un texto en negritas (_bold_ en ingles) y puedes ver como se abre y se cierre:
```html
Eso es en <b>negritas</b>
```
 

## Estructura del documento HTML `<html>`, `<head>` y `<body>`

Cada documento HTML tiene la siguiente estructura:
```html
<html>
  <head>
  </head>
  <body>
  </body>
</html>
```
En la cabeza, _head_ van informaciónes sobre el documento, como en titulo. En el cuerpo, _body_ va todo el contendio. Y los dos estan, bien ordenado en el elemento `<html>`.

### Attributos `lang` y `charset`
El navegador necesita algunas informaciónes mas para poder trabajar bien con nuestro documento. Cómo lo han visto en el capítulo sobre el [index.html](./index_html.md) se agregan unos detalles. Aquí repetimos ese ejemplo:

```html
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Mi proyecto chido</title>
  </head>
  <body>
    Hola mundo, este es mi proyecto chido!
  </body>
</html>
```

La primera línea avisa que se trata de un documento HTML. 

`lang` es un attributo del elemento `<html>` y el valor `es` avisa al navegador que nuestro documento esta escrito en español.

`charset` aviso al navegador que _encoding_ tiene el archivo. Hoy el estandard y la recomendación es usar `utf-8`.

## Encabezados `<h1>` a `<h6>`

Los elementos `<h1>` hasta `<h6>` implementan seis niveles de encabezado del documento. `<h1>` es el más importante, `<h6>` el menos importante.

Ejemplo:
```html
<h1>encabezado 1</h1>
<h2>encabezado 2</h2>
<h3>encabezado 3</h3>
<h4>encabezado 4</h4>
<h5>encabezado 5</h5>
<h6>encabezado 6</h6>
```

Resulta en algo así:
<blockquote>
<h1>encabezado 1</h1>
<h2>encabezado 2</h2>
<h3>encabezado 3</h3>
<h4>encabezado 4</h4>
<h5>encabezado 5</h5>
<h6>encabezado 6</h6>
</blockquote>

:-)

## Párrafo `<p>`
Como en cada texto mas grande es recomendable organizar el texto en párrafos. El texto entre `<p>` y `</p>` es un párrafo.

## Listas con `<ol>`, `<ul>` y `<li>`

Para listas hay dos opciones, con puntos _unordered list_ `<ul>` o con números _ordered list_ `<ol>`. `<li>` representa un _list item_.

Ejemplo:
```html
<ol>
 <li>punto uno</li>
 <li>punto dos</li>
 <li>punto tres</li>
</ol>

o sin orden:
<ul>
 <li>punto uno</li>
 <li>punto dos</li>
 <li>punto tres</li>
</ul>
```

Resulta en algo así:
<blockquote>
<ol>
 <li>punto uno</li>
 <li>punto dos</li>
 <li>punto tres</li>
</ol>

o sin orden:
<ul>
 <li>punto uno</li>
 <li>punto dos</li>
 <li>punto tres</li>
</ul>
</blockquote>

## Línea horizontal `<hr>`

El nombre del elemento viene de _horizontal ruler_. 

Ejemplo:
```html
arriba
<hr/>
abajo
```

Resulta en algo así:
<p><blockquote>
arriba
<hr>
abajo</blockquote>
</p>
Notaste algo especial? Este elemento no tiene contendio. Entonces podemos abrirlo y cerrarlo en una vez. Para eso usamos la forma con el `/` antes del `>`.

Los navegadores no son tan estrictos cómo por ejemplo el compilador o interpretador de un lenguaje de programación. Por eso te permiten en algunos casos de olvidar el tag que cierra el elemento y el documento se ve bien. Pero es considerado buen estilo de cerrar los elementos siembre.

Con los elementos que aprendimos ahora ya podemos hacer documentos complejos. Por favor usa a todos los elementos de esta capítulo en tu proyecto y subalo a GitLab.