# Primeros pasos en Ubuntu

Ubuntu tiene una superficie o _user interface_ (_UI_) que se parece mucho a la de otros sistemas operativos. Puedes explorar sin miedo los diferentes menús y ver qué programas están instalados. Seguramente hay un navegador, Firefox, el cual vamos a utilizar mucho.

Vamos a usar el concepto de _keyboard shortcuts_ o _key bindings_. Ese concepto se refiere a usar el teclado para mandar comandos a tu programa. Lo usamos mucho por dos razones. Primero, es mas rápido que buscar el lugar correcto con el mouse y hacer el click. Segundo es mas fácil explicar una acción en un manual como este con keyboard shortcuts que decir "busca el icono negrito que se parece a una pantalla y haz click ahí".

Por ejemplo, tenemos `Ctrl+Alt+T` para abrir el terminal. El terminal lo vamos a necesitar casi siempre, entonces ya puedes ir aprendiendo este shortcut.

Luego muchos teclados tienen una "tecla Windows", normalmente está cerca de `Alt` y esta marcado con la bandera de Windows. La llamamos `Win`. Con `Win` se abre una especie de búsqueda, donde encuentras entre otras cosas los programas instalados. Entonces si haces click en `Win` se abre el buscador, si escribes `firefox` y luego presionas la tecla `Enter` se abre el navegador Firefox. Con `Win` + `files` se va a abrir el equivalente al File Explorer de Windows.

Si abrimos tantas ventanas, también tenemos que cerrarlas. Usa `Alt+F4` para cerrar ventanas.

Con varias ventanas abiertas puedes buscar entre todas con `Alt+Tab`. Con cada `Tab` más elijes la siguiente ventana.

Juega un poco con estos shortcuts.

