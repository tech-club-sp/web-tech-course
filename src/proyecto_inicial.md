# Proyecto inicial

Ya que tenemos un editor, podemos empezar con nuestro proyecto. Vamos a hacer una página web muy sencilla para aplicar los conceptos que vamos a aprender. En este ejercicio usaremos textos cortos y poco interesantes. Pero puedes jugar con otros contenidos y hacer por ejemplo una página sobre un interés tuyo o simplemente presentarte a ti.
