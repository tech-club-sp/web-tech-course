# Instalar Linux

## ¿Por qué Linux?

El internet corre en su gran mayoría (> 90%) con servidores que tienen instalado Linux. Como desarrollador tiene una ventaja trabajar en el mismo sistema en que va a correr nuestro software. Además Linux es gratuito, rápido, flexible y divertido. Con eso dicho, se puede también desarrollar con Windows (Microsoft) o OS X (Apple) aunque las instrucciones en este curso están hechas para Linux. Se pueden fácilmente adaptar a OS X, porque es también parte de la gran familia de los Unixes (así como Linux), y con algunas adaptaciones también a Windows.

## Instalamos un Linux: Ubuntu

De Linux hay muchas variaciones, se llaman _distribuciones_. Para tener una idea puedes checar <https://distrowatch.com/> donde puedes ver las primeras 100 distribuciones. 

Aquí recomendamos Ubuntu porque es fácil de instalar y mantener, y tiene una base de usuarios grande. Si tienes ya Windows en tu computadora te recomendamos instalar Ubuntu **al lado** de Windows. Así puedes volver a usar Windows cuando lo necesites.

Aquí no vamos a repetir las instrucciones, sino que vamos a compartir unos enlaces que te podrían ayudar:
- <https://www.youtube.com/watch?v=AR2F09BQgDY> (el Ubuntu actual es 20.04, pero los pasos siguen siendo los mismos).
- en caso de que el installer no te ofrezca todo el espacio libre, puedes reducir el espacio de tu partición en Windows: <https://www.tecmint.com/install-ubuntu-alongside-with-windows-dual-boot/> después de haber reducido el espacio de tu partición de Windows es recomendable reiniciar tu sistema antes de empezar con la instalación de Ubuntu.
