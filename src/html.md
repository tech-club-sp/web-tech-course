# HTML

HTML viene del ingles _Hypertext Markup Language_. Es el lenguaje en que se escribe una página web. 

## Hypertext

Cuando empezó en 1991 las páginas eran textos sin muchas extras. Un factor de exito era un nuevo concepto: los enlaces, o _links_ en ingles. Esos enlaces permiten de saltar a otros partes en la misma página o de cargar una nueva página que hasta puede ser de otro servidor. Con enlaces hacemos de un texto un 'hipertexto'.

## Markup

HTML describe como formatear un texto. Estos describciones se llaman markup o elementos. Cada elemento dice al navegador como hay que presentar el siguiente texto, imagen o otro contendio.

Vamos a ver los elementos mas comunes y sus efectos en el capítulo siguiente.

### Otros fuentes en español

- Curso de Microsoft sobre [HTML5](https://docs.microsoft.com/es-es/previous-versions/msdn10/Hh749019(v=MSDN.10))
- Material sobre [Desarollo Web de Mozilla](https://developer.mozilla.org/es/docs/Learn)