# Ver nuestra página en el navegador

Ahora nos falta ver el resultado de tanto trabajo. Una página web se usa con un navegador, ¿verdad? Vamos a verlo. En el terminal, que todavía tienes abierto, escribe
```shell
firefox index.html
```
Se va a abrir el navegador Firefox y nos va a mostrar la página que acabamos de crear. Arriba, en la barra del URL puedes ver algo como `file:///home/tu_usuario/mi_proyecto_chido/index.html`. 

Quizás te has dado cuenta que otras páginas inician con `https://` o algo parecido. La diferencia es que nuestra página vive (todavía) en nuestro disco duro, pero otras páginas normalmente están en un servidor en el internet. En unos capítulos vamos a subir la página a un servidor!

¿Encontraste dónde se muestra el titulo de la página? Lo hemos especificado en el código que escribimos en nuestro editor con esta línea

```html
<title>Mi proyecto chido</title>
```
¿Lo encontraste? Es el título en la pestaña del navegador.

## Ver cambios en el navegador

Podemos ahora cambiar el texto de la página en el editor. Por ejemplo, agregando alguna frase después de `Hola mundo, este es mi proyecto chido!` No olvides de guardar los cambios con `Ctrl+S`. Para ver estos cambios en el navegador puedes usar `F5` o el botón con la flecha circular, ambas opciones van a recargar la página.
