# Crear un repository

Git llama la carpeta de tu proyecto el _directorio de trabajo_ o en inglés _working directory_. En la carpeta de tu proyecto ejecutamos lo siguiente.  
Requerda que `mi_proyecto_chido` es un ejemplo, por favor sé creativo y pon un nombre único que describa bien tu proyecto: 

```
cd mi_proyecto_chido
git init
```
Ahora ya tienes un _local_ repository. ¿Dónde está? 

Se está escondiendo un poco. Si ejecutas `ls` no lo vas a ver. Pero con `ls -a` puedas ver una carpeta `.git`. No tienes que preocuparte por el contenido, pero es importande saber que esta carpeta representa el local repository.

### Intermezzo: archivos con punto `.` y `ls -a`

Nombres de archivos y carpetas que inician con `.` están invisibles en modo normal. Por eso no podíamos ver a `.git` con el comando `ls`. Agregando la opción `-a` de _all_ podemos ver todo en una carpeta. El punto se usa para proteger a archivos importantes y para archivos que no tienen que estar visibles en el trabajo del día al día.

Ahora seguimos con Git!

## Remote repository

Para usar todas las ventajas de Git agregamos un repository remoto. Si llegas a este punto, me puedes mandar tu correo electronico y te voy a agregar al grupo [https://gitlab.com/tech-club-sp/member_reps](https://gitlab.com/tech-club-sp/member_reps).

En cuando te llegó la invitación al grupo y hiciste una nueva cuenta en gitlab puedas seguir. 

Ahora puedes usar el botón `New Project` en el grupo `member_reps` y elegir 'blank project' y luego llenar el campo `Project name` con el nombre de tu proyecto. Recomiendo que sea el nombre de tu working directory. Para terminar la creación del remote repository confirmas con `Create project`.

En la página que sigue a la creación del repository remoto puedas ver varios comandos. Copia los que se parecen a los de abajo (va a cambiar tu nombre y el nombre del repository):

```shell
# Git global setup
git config --global user.name "Fulano"
git config --global user.email "47383-fulano@users.noreply.gitlab.com"
...

# Push an existing folder
git remote add origin git@gitlab.com:tech-club-sp/member_reps/rep_name.git
git add .
git commit -m "Initial commit"
git push -u origin master
...

```
En un terminal y en la carpeta `mi_proyecto_chido` ejecuta estas 6 líneas de comandos copiados.

Si tenías éxito vas a ver algo parecido a:
```
[master (root-commit) d12d6ba] Initial commit
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 test
Counting objects: 3, done.
Writing objects: 100% (3/3), 207 bytes | 207.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To gitlab.com:tech-club-sp/member_reps/mi_proyecto_chido.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
➜  mi_proyecto_chido git:(master) 
```

Vamos a explicar paso por paso lo que hicimos. 

Cada cambio que se guarda en Git tiene un nombre y un email asociado. En este caso no usamos el correo nuestro pero un correo que nos da GitLab (eso es para protegernos de maluso de nuestro correo electrónico). Ese configuración la hacemos una vez y sólo la repetimos si nos cambiamos de computadora.

```shell
git config --global user.name "Fulano"
git config --global user.email "47383-fulano@users.noreply.gitlab.com"
```

Hay que conectar el repository local con el repository remoto. Eso lo hicimos con la siguiente línea.
```shell
git remote add origin git@gitlab.com:tech-club-sp/member_reps/rep_name.git
```

Para terminar hemos agregado todo el contenido de nuestra carpeta y lo mandamos al repository remoto. En el siguiente capítulo explicamos este proceso a más detalle, porque lo vamos a usar mucho.

