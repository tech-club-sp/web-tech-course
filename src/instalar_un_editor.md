# Instalar un Editor

Para poder programar necesitamos un programa con el que podamos escribir código y guardarlo en un archivo. Hay muchas variedades de editores. Muy sencillos y muy complejos, que hacen mucho más que sólo editar texto. Aquí usaremos [Visual Studio Code](https://code.visualstudio.com/). Tiene mucha funcionalidad, se presta para desarrollar para la web y es gratuito. 

En tu terminal (acuerda `Ctrl+Alt+T`) ejecuta el siguiente comando: 
```shell
sudo snap install code --classic
```
Te va a pedir que proporciones tu contraseña.

Tendrías que ver algo como `code ff915844 from Visual Studio Code (vscode) installed`. 

Explicamos un poco lo que sucedió: Se bajó el programa del internet (sin conexión no hubiera funcionado), se instaló y esta listo para usarlo - tan fácil! 

En el código escribimos `sudo`, esto hace que el resto del comando se ejecute con 'super poderes'. Cuidado con comandos que empiecen con `sudo`. Pueden instalar, pero también borrar software o destruir algo de tu sistema. Por esa razón se necesita la contraseña. `snap` es una de las formas como instalamos software en nuestro Ubuntu. 

Puedes intentar arrancar el editor con el shortcut: `Win` y teclear `code`. O simplemente en un terminal escribes `code`y le das `Enter`.

En caso de que quieras explorar otros editores puedes empezar con dos muy famosos: [Vim](https://www.vim.org/) y [Emacs](https://www.gnu.org/software/emacs/). Vim probablemente ya está instalado y lo puedes arrancar con el comando `vi` en un terminal. O puedes probar una IDE (Integrated Development Environment) como [Webstorm](https://www.jetbrains.com/webstorm/).
